<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <?php
        //Part 1
        $yourName = "James Borlaug";

        //Part 2
        echo "<h1>PHP Basics</h1>";



    ?>
</head>

<body>
    <!--Part 3-->
    <h2>
        <?php echo $yourName;?>
    </h2>

    <?php
        //Part 4
        $number1 = 3425;
        $number2 = 7860;
        $total = $number1 + $number2;

        //Part 5
        echo "<p>" . $number1 . " + " . $number2 . " = " . $total . "</p>";
    ?>

    <!--Part with no-number-->
    <script>
        <?php
            echo "var languages = [\"PHP\", \"HTML\", \"Javascript\"];";
            echo "document.write(languages);";?>
    </script>
</body>

</html>
