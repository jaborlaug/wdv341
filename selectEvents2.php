<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Unit 13</title>

<?php
  require "connection.php";
  //echo "Connected successfully<br/>";
  $stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_presenter, event_time, event_day FROM wdv341_events");

  $stmt->execute();
  //echo "Statement executed<br/>";

  $msgOut = $_GET['msgOut'];
  if (isset($msgOut)) {
    if($msgOut == 'error'){
      echo "Error! Record has not been deleted";
    } else{
      echo "Record $msgOut has been deleted";
    }
  }
?>

</head>
<body>
  <h1>WDV 341 - php</h1>
  <h2>SELECT statement</h2>

  <table>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Description</td>
      <td>Presenter</td>
      <td>Time</td>
      <td>Day</td>
      <td></td>
    </tr>
    <?php
      while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $id=$row['event_id'];
        //echo "In while loop";
        echo "<tr>";
          echo "<td>" . $row['event_id'] . "</td>";
          echo "<td>" . $row['event_name'] . "</td>";
          echo "<td>" . $row['event_description'] . "</td>";
          echo "<td>" . $row['event_presenter'] . "</td>";
          echo "<td>" . $row['event_time'] . "</td>";
          echo "<td>" . $row['event_day'] . "</td>";
          echo "<td><a href='deleteEvent.php?event_id=$id'>Delete</a></td>";
        echo "</tr>";
      }
    ?>
  </table>
</body>
</html>
