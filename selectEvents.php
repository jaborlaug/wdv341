<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Unit 13</title>

<?php
  require "connection.php";
//  $serverName = "wdv341.cbg7uz5h6fti.us-east-1.rds.amazonaws.com";
//  $username = "masterjames";
//  $password = "dbPassword";
//  $database = "wdv341";
  //echo "Something is actually happening when this page loads<br/>";
//  try {
//      $conn = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
      // set the PDO error mode to exception
//      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //echo "Connected successfully<br/>";
      $stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_presenter, event_time, event_day FROM wdv341_events");
      //$conn->exec($stmt);
      $stmt->execute();
      //echo "Statement executed<br/>";
//  }
//  catch(PDOException $e){
//    echo "Connection failed: " . $e->getMessage();
//  }

?>

</head>
<body>
  <h1>WDV 341 - php</h1>
  <h2>SELECT statement</h2>

  <table>
    <tr>
      <td>ID</td>
      <td>Name</td>
      <td>Description</td>
      <td>Presenter</td>
      <td>Time</td>
      <td>Day</td>
    </tr>
    <?php
      while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        //echo "In while loop";
        echo "<tr>";
          echo "<td>" . $row['event_id'] . "</td>";
          echo "<td>" . $row['event_name'] . "</td>";
          echo "<td>" . $row['event_description'] . "</td>";
          echo "<td>" . $row['event_presenter'] . "</td>";
          echo "<td>" . $row['event_time'] . "</td>";
          echo "<td>" . $row['event_day'] . "</td>";
        echo "</tr>";
      }
    ?>
  </table>
</body>
</html>
