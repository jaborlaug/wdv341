<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;
}
</style>

<script>
	function clearPage(){
		location.reload();
	}

</script>

<?php
	//Initialize variables for form's inputs
	$inName = $inEmail = $radioGroup1 = "";
	$radioGroupChecked = array("Phone"=>"","Email"=>"","USMail"=>"");
	//Variable to display submission status
	$submissionMessage = "";


	if($_SERVER["REQUEST_METHOD"] == "POST"){

		//Assign values from text boxes
		$inName = $_POST["inName"];
		$inEmail = $_POST["inEmail"];
		$radioGroup1 = $_POST["RadioGroup1"];
		//Assign 'checked'
		$radioGroupChecked[$radioGroup1] = "checked";
		//Submission confirmation message
		$submissionMessage = "Form has been submitted";
	}

?>

</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment


</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="Unit-7.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo $inName; ?>"/></td>
      <td width="210" class="error"></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inEmail" id="inEmail" size="40" value="<?php echo $inEmail; ?>" /></td>
      <td class="error"></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_0" value="Phone" <?php echo $radioGroupChecked["Phone"] ?>>
          Phone</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_1" value="Email" <?php echo $radioGroupChecked["Email"] ?>>
          Email</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_2" value="USMail" <?php echo $radioGroupChecked["USMail"] ?>>
          US Mail</label>
        <br>
      </p></td>
      <td class="error"></td>
    </tr>
	</table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
		<input type="reset" name="button2" id="button2" value="Clear Form" onclick="clearPage()"/>
		<?php echo " " . $submissionMessage; ?>
  </p>
</form>
</div>

</body>
</html>
