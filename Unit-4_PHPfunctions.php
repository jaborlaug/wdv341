<?php
  //1. Create a function that will accept a date input and format it into mm/dd/yyyy foramt.
  $formatUSDate = "2018-04-03";

  function formatDateMDY($inDate){
    $date = date_create($inDate);
    return date_format($date, "d/m/Y");
  }

  //2. Create function that will accept a date and format into dd/mm/yyyy to use w/ international date_date_set
  $formatIntlDate = "2017-04-03";

  function formatDateDMY($inDate){
    $date = date_create($inDate);
    return date_format($date, "m/d/Y");
  }

  //3. Create function that will accept input string.  It will do the following:
  $formatString = "   Format this string dmcc ";

  function stringFunction($inString){
    //a. Display the number of characters in the string
    echo "The string has " . strlen($inString) . " characters. <br />";
    //b. Trim any leading or trailing whitespace
    echo "Without leading or trailing whitespace: " . trim($inString) . "<br />";
    //c. Display the string as all lowercase characters
    $lowerString = strtolower($inString);
    echo "All lowercase: " . $lowerString . "<br />";
    //d. Will display whether or not the string contains "DMACC" in either case
    if(strpos($lowerString, "dmacc") != false){
      echo "String contains 'DMACC'";
    } else{
      echo "String does not contain 'DMACC'";
    }
  }

  //4. Create a function that will accept a number and display it as a formatted number.  Use 1234567890 for testing
  $formatWithCommas = 1234567890;

  function formatNumber($inNumber){
    $frmtNumber = number_format($inNumber);

    echo $frmtNumber;
    }

  //5. Create a function that will accept a number and display it as US currency
  $formatAsDollars = 1234567890;

  function formatCurrency($inCurr){
    $frmtCurr = number_format($inCurr,2);

    echo ("$" . $frmtCurr);
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
  <!-- 1. -->
  <p>
    <?php echo formatDateMDY($formatUSDate); ?>
  </p>

  <!-- 2. -->
  <p>
    <?php echo formatDateDMY($formatIntlDate); ?>
  </p>

  <!-- 3. -->
  <p>
    <?php echo stringFunction($formatString); ?>
  </p>
  <!-- 4. -->
  <p>
    The number <?php echo $formatWithCommas; ?> should be formatted as:  <?php formatNumber($formatWithCommas); ?>
  </p>

  <!-- 5. -->
  <p>
    The dollar amount <?php echo $formatAsDollars; ?> should be formatted as: <?php formatCurrency($formatAsDollars); ?>
  </p>
</form>
</body>
</html>
