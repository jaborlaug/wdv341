<?php
session_start();

$user_name = $user_pwd = "";
$validPwd = false;

if(isset($_POST["logout"])){
  $_SESSION['validUser'] = "no";
}

if ($_SESSION['validUser'] !== "yes") {
	$userStatus = "Please sign in";

  if(isset($_POST["submit"])){
    $user_name = $_POST['user_name'];
    $user_pwd = $_POST['user_pwd'];

    require 'connection.php';

    try {
      $stmt = "SELECT user_name, user_pwd FROM wdv341_users WHERE user_name = '$user_name';";
      $stmt = $conn->prepare($stmt);
      $stmt->execute();

      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $user_id = $row['user_id'];
        $user_name = $row['user_name'];
        $user_pwd_fromDB = $row['user_pwd'];
      }

      if ($user_pwd_fromDB == $user_pwd){
        $_SESSION['validUser'] = "yes";
        $_SESSION['user_name'] = $user_name;
        $userStatus = "Welcome " . $_SESSION['user_name'] . ".<br /><a href='list.php'>Continue to List</a>";

      } else {
        $userStatus = "Username and Password to not match!";
      }
    } catch (PDOException $e){
    }
  }
} else {
  $userStatus = "You are signed in<br /><a href='list.php'>Continue to List</a>";
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="login.php" method="post" name="loginForm">
      <p id="userStatus">
        <?php echo $userStatus; ?>
      </p>
      <p>
        <label for="user_name">User Name:</label>
        <input type="text" name="user_name">
      </p>
      <p>
        <label for="user_pwd">Password:</label>
        <input type="password" name="user_pwd">
      </p>
      <p>
        <input type="submit" name="submit" value="Sign In">
        <input type="submit" name="logout" value="Sign Out">
      </p>

    </form>
  </body>
</html>
