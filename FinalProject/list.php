<?php
  session_start();
  if ($_SESSION['validUser'] != "yes"){
    header("Location: login.php");
  }

  require "connection.php";

  $stmt = $conn->prepare("SELECT list_id, list_completed, list_title, list_description, list_day, list_date FROM wdv341_list");
  $stmt->execute();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table>
      <tr>
        <td rowspan="2">Done</td>
        <td>Title</td>
        <td>Day</td>
        <td>Date</td>
        <td rowspan="2"></td>
        <td rowspan="2"></td>
      </tr>
      <tr>
        <td colspan="3">Description</td>
      </tr>
      <tr></tr>
      <?php
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          $list_id = $row['list_id'];
          echo "<tr>";
            echo "<td rowspan='2'>" . $row['list_completed'] . "</td>";
            echo "<td>" . $row['list_title'] . "</td>";
            echo "<td>" . $row['list_day'] . "</td>";
            echo "<td>" . $row['list_date'] . "</td>";
            echo "<td rowspan='2'><a href='updateList.php?list_id=$list_id'>Update</a></td>";
            echo "<td rowspan='2'><a href='deleteList.php?list_id=$list_id'>Delete</a></td>";
          echo "</tr>";
          echo "<tr>";
            echo "<td colspan='3'>" . $row['list_description'] . "</td>";
            echo "</tr>";
        }
      ?>
      <tr>
        <td><input type="button" name="AddItem" value="Add Item" onclick="location.href='insertList.php'"></td>
        <td><input type="button" name="toLogin" value="Return to Login" onclick="location.href='login.php'"></td>
      </tr>
    </table>
  </body>
</html>
