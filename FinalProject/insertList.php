<?php
  session_start();
  if ($_SESSION['validUser'] != "yes"){
    header("Location: login.php");
  }

  $list_id = $list_title = $list_description = $list_day = $list_date = "";

  if($_SERVER["REQUEST_METHOD"] == "POST"){
    //echo "REQUEST_METHOD is POST<br />";
    $list_id = $_POST['list_id'];
    $list_title = $_POST['list_title'];
    $list_description = $_POST['list_description'];
    $list_day = $_POST['list_day'];
    $list_date = $_POST['list_date'];

    require "connection.php";
//    INSERT INTO  wdv341_list (list_completed, list_title, list_day, list_date)
//    VALUES (1, 'PHP Final','Monday', '20180506')

    $stmt = "INSERT INTO wdv341_list (list_completed, list_title, list_description, list_day, list_date) ";
    $stmt .= "VALUES ('$list_completed', '$list_title', '$list_description', '$list_day', '$list_date')";
    //echo "stmt created<br />";
    $stmt = $conn->prepare($stmt);
    //echo "stmt prepared <br />";
    try {
    $stmt->execute();
    //echo "stmt executed successfully <br />";
  } catch(PDOException $e){
    //echo $e;
  }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script>
      function goToList() {

      }
    </script>
  </head>
  <body>
    <form action="insertList.php" method="post" name="addItemForm">
      <table>
        <tr>
          <td>Done</td>
          <td>Title<span class="validation" id="list_title">*</span></td>
          <td>Day</td>
          <td>Date</td>
        </tr>
        <tr>
          <td> <input type="checkbox" name="list_completed" checked="<?php echo $list_completed ?>"></td>
          <td> <input type="text" name="list_title" value="<?php echo $list_title ?>"> </td>
          <td> <input type="text" name="list_day" value="<?php echo $list_day ?>"> </td>
          <td> <input type="text" name="list_date" value="<?php echo $list_date ?>"> </td>

        <tr>
          <td colspan="4">Description</td>
        </tr>
        <tr>
          <td colspan="4"><input type="text" name="list_description" value="<?php echo $list_description ?>"></td>
        </tr>
      <table>
        <input type="submit" name="submit" value="Add Item">
        <input type="reset" name="reset" value="Clear Entries">
        <input type="button" name="toList" value="Return to list" onclick="location.href='list.php'">
    </form>
  </body>
</html>
