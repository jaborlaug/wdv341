<?php
  session_start();
  if ($_SESSION['validUser'] != "yes"){
    header("Location: login.php");
  }
  $recToDelete = $_GET['list_id'];
  $msgOut = "Record $recToDelete ";
  require "connection.php";
  try{
    $stmt = "DELETE FROM wdv341_list WHERE list_id = $recToDelete;";
    $stmt = $conn->prepare($stmt);
    $stmt->execute();
    $msgOut .= "deleted successfully";
  } catch(PDOException $e){
    //echo $e;
    $msgOut .= "not deleted successful";
  }


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script>
    </script>
  </head>
  <body>
    <h2><?php echo $msgOut; ?></h2>
    <input type="button" name="toList" value="Return to list" onclick="location.href='list.php'">
  </body>
</html>
